﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tester : MonoBehaviour {

	public Text text;
	public AudioSource audioSource;
    public Image image;

    private bool isPressed = false;

    private static Tester instance;
    public static Tester Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType<Tester>();
            }
            return instance;
        }
    }

    private void Update() {
        if (isPressed) {
            isPressed = false;
        } else if (!isPressed) {
            image.color = Color.white;
        }
    }

    public void StartNativeTouch() {
        if (Application.platform == RuntimePlatform.IPhonePlayer) {
            Debug.Log("Start native touch!!");
		    NativeTouch.StartNativeTouch();
        } else {
            return;
        }
	}

	public void Test() {
		text.text = IosNativeAudio.Test(4).ToString();
	}
    
	public void LoadSound() {
		IosNativeAudio.LoadSound();
	}

	public void PlaySound() {
        IosNativeAudio.PlaySound();

        isPressed = true;

        if (isPressed)
            image.color = Color.red;
	}

}