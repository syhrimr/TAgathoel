﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class UIManager : MonoBehaviour {

    #region VARIABLES

	[Header("External Components")]
    public AudioManager audioManager;
    [SerializeField] private Animator animLoad;
    [Header("UI Elements")]
    public Slider volumeSlider;
    public Text   textScale;
//    public Text instrumentText;
    [Header("Show Hidden Components")]
    public GameObject imgGameobject;
    public GameObject imgShow;
    public GameObject imgHidden;
    public GameObject imgLoading;
    [Header("Tutorial Panel")]
    public GameObject tutorialPanel;
    [Header("Images Array")]
    public Image[] images;
    public Image[] invImages;
    [Header("Sprites Array")]
    [SerializeField] private Sprite[] slendro;
    [SerializeField] private Sprite[] pelog;

    private int cScale = 1;
    private int isShow = 0;
	private int length = 0;
    private Image imageLoad;

    #endregion

    #region EVENTS

    private void Start() {
        if (volumeSlider == null)
            return;
        if (tutorialPanel == null)
            return;

        volumeSlider.value = volumeSlider.maxValue;
        tutorialPanel.active = false;
        imgLoading.active = false;
        imageLoad = imgLoading.GetComponent<Image>();
    }

    #endregion

    #region CHANGE SCENE

    public void GoToScene(int scene) {
        if (imgLoading != null) {
            imgLoading.active = true;
//            StartCoroutine(FadeInPanel(imageLoad, 1f));
        }

        SceneManager.LoadScene(scene);
    }

    public void GoToScene(string scene) {
        if (imgLoading != null) {
            imgLoading.active = true;
//            StartCoroutine(FadeInPanel(imageLoad, 1f));
        }

        SceneManager.LoadScene(scene);
    }

 //   private static IEnumerator FadeInPanel(Image _image, float _time) {
 //       _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, _image.color.a + (Time.deltaTime * _time));
 //       yield return null;
 //   }

    #endregion

    #region CHANGE SCALE TEXT

    public void ChangeScaleState() {
		length = images.Length;
		
        if (cScale == 0) {
            audioManager.listScale = Scale.Slendro;

            for (int i = 0; i < length; i++) {
                images[i].sprite = slendro[i];

                if (audioManager.listInstrument == Instrument.BonangBarung ||
                    audioManager.listInstrument == Instrument.BonangPenerus)
                    invImages[i].sprite = slendro[i];
            }

            textScale.text = "Slendro";
            cScale = 1;
        } else if (cScale == 1) {
            audioManager.listScale = Scale.Pelog;

            for (int i = 0; i < length; i++) {
                images[i].sprite = pelog[i];

                if (audioManager.listInstrument == Instrument.BonangBarung ||
                    audioManager.listInstrument == Instrument.BonangPenerus)
                    invImages[i].sprite = pelog[i];
            }

            textScale.text = "Pelog";
            cScale = 0;
        }
    }

    #endregion

    #region SHOW/HIDE SCALE TEXT

    public void ShowHideButtonControl() {
        if (isShow == 0) {
            imgShow.SetActive(true);
            imgHidden.SetActive(false);
            ShowText();
            isShow = 1;
        } else if (isShow == 1) {
            imgShow.SetActive(false);
            imgHidden.SetActive(true);
            HideText();
            isShow = 0;
        }
    }

    private void ShowText() {
        imgGameobject.SetActive(false);
    }

    private void HideText() {
        imgGameobject.SetActive(true);
    }

    #endregion

    #region CHANGE VOLUME VALUE

    public void VolumeChange() {
        audioManager.volume = volumeSlider.value;
    }

    #endregion

    #region SHOW TUTORIAL PANEL

    public void OnClickToShow() {
        tutorialPanel.active = true;
    }

    public void OnClickToClose() {
        tutorialPanel.active = false;
    }

    #endregion
}