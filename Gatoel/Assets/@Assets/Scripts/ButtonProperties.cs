﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ButtonProperties {
    public int index;
//    public Image image;
    public AudioSource audio;
    public AudioClip[] clips;
}
