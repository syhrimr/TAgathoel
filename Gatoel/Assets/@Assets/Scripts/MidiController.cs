﻿using UnityEngine;
using UnityEngine.UI;
using MidiJack;

public class MidiController : MonoBehaviour {

    [SerializeField] private int noteNumber;
    [SerializeField] private int vKnobNumber = 7;
    [SerializeField] private int sKnobNumber = 16;
    [SerializeField] private AudioSource audioSource;

    public AudioManager am;
    public Slider volumeSlider;

    private void Awake() {
        audioSource = GetComponent<AudioSource>();
        //volumeSlider.value = 1;
    }

    private void Start() {
        //Debug.Log(MidiStateUpdater.);
    }

    private void Update() {
        #if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
        PlaySoundInMidi();
        //ChangeVolumeByKnob();
        #endif
    }

    private void PlaySoundInMidi() {
        if (MidiMaster.GetKeyDown(noteNumber + 48)) {
            audioSource.volume = am.volume / 2;
            audioSource.volume = MidiMaster.GetKey(noteNumber + 48);
            audioSource.Play();
        }
    }

    private void ChangeVolumeByKnob() {
        var v = MidiMaster.GetKnob(vKnobNumber);

        volumeSlider.value = v;
    }
}