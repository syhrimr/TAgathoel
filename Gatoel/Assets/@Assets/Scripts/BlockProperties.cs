﻿using UnityEngine;

[System.Serializable]
public class BlockProperties {
    public int index;

    public float slendroFq = 0;
    public float pelogFq = 0;

}