using UnityEngine;

public class FrequencyInitiator : MonoBehaviour {

    #region VARIABLES

    [Header("Reference")]
    [SerializeField] private float frequency = 440;
    [Header("Frequency Data")]
    public BlockProperties[] blockProperties = null;
    
    private float m = 0f;
    private float n = 0f;
	private int length = 0;

    #endregion

    #region SCALE FREQUENCIES

    private float SlendroScale(int _index, float _fq) {
        m = (_index - 6);
		
        n = Mathf.Pow(2f, m / 5);
		
        return n * _fq;
    }

    private float PelogScale(int _index, float _fq) {
        switch (_index) {
            case 0: m = (float)-((4 * 150) + (2 * 225)); break;
            case 1: m = (float)-((4 * 150) + 225); break;
            case 2: m = (float)-((3 * 150) + 225); break;
            case 3: m = (float)-((2 * 150) + 225); break;
            case 8: m = 375.0f; break;
            default: m = (float)(_index - 6) * 150; break;
        }

        n = (float)Mathf.Pow(2f, m / 1200);
		
        return n * _fq;
    }

    #endregion

    #region INITIATE FREQUENCIES

    private void FrequencyBalungan(float _fq) {
        blockProperties = new BlockProperties[9];

		length = blockProperties.Length;
		
        for (int i = 0; i < length; i++) {
            blockProperties[i] = new BlockProperties();
			
            blockProperties[i].index = i;
            blockProperties[i].slendroFq = SlendroScale(i, _fq);
            blockProperties[i].pelogFq = PelogScale(i, _fq);
        }
    }

    private void FrequencyBonang(float _fqTop, float _fqBtm) {
        blockProperties = new BlockProperties[16];

		length = blockProperties.Length;
		
        for (int i = 0; i < length; i++) {
            blockProperties[i] = new BlockProperties();

            blockProperties[i].index = i;

            if (i < 8) {        // Handle top notes
                blockProperties[i].slendroFq = SlendroScale(i, _fqTop);
                blockProperties[i].pelogFq = PelogScale(i, _fqTop);
            } else {            // Handle bottom notes
                blockProperties[i].slendroFq = SlendroScale(i - 8, _fqBtm);
                blockProperties[i].pelogFq = PelogScale(i - 8, _fqBtm);
            }
        }
    }

    public void InitFrequency(Instrument _inst) {
        float fq = 0;
        float fqBonangTop = 0;
        float fqBonangBtm = 0;

        switch (_inst) {
            case Instrument.Saron:
                fq = frequency;
                break;
            case Instrument.Peking:
                fq = frequency * 2;
                break;
            case Instrument.Demung:
                fq = frequency / 2;
                break;
            case Instrument.BonangBarung:
                fqBonangTop = frequency;
                fqBonangBtm = frequency / 2;
                break;
            case Instrument.BonangPenerus:
                fqBonangTop = frequency * 2;
                fqBonangBtm = frequency;
                break;
            default:
                return;
        }

        if (_inst != Instrument.BonangBarung && _inst != Instrument.BonangPenerus)
            FrequencyBalungan(fq);
        else
            FrequencyBonang(fqBonangTop, fqBonangBtm);
    }

    #endregion

}