﻿using UnityEngine;
using UnityEngine.UI;
using System;

[RequireComponent(typeof(AudioSource))]
public class AudioTestManager : MonoBehaviour {
    [SerializeField] private AudioSource audio;
    [SerializeField] private int bufferLength;
    [SerializeField] private int numBuffer;
    [SerializeField] private float[] arrayLatency;
    [SerializeField] private float latency;
    [SerializeField] private bool buttonPressed = false;

    private float total = 0;
    private int i = 0;
	private int j = 1;

    private Image img;

    public Text textLatency;
    public Text[] debugArray;
    public Text debug;

    private void Awake() {
        arrayLatency = new float[10];

        AudioSettings.GetDSPBufferSize(out bufferLength, out numBuffer);
        var sampleRate = AudioSettings.outputSampleRate;
        var estLatency = (float)bufferLength / sampleRate;

        audio = GetComponent<AudioSource>();
        audio.playOnAwake = false;
        audio.loop = false;

        debug.text = "BufferSize: " + bufferLength +
            "\nSampling Rate: " + sampleRate + " Hz" +
            "\nEst: " + estLatency * 1000 + " ms";

        img = GetComponent<Image>();
    }
	
	private void Update() {
		if (buttonPressed) {
			float lat = 0;

			lat += Time.deltaTime;

			if (audio.isPlaying) {
				lat *= 1000;

				arrayLatency[i] = lat;
				debugArray[i].text = lat.ToString() + " ms";
				total += arrayLatency[i];
				
				latency = total / j;
				textLatency.text = latency.ToString() + " ms";

				buttonPressed = false;
				i++;
				j++;
			}

			if (i >= 10) {
				i = 0;
			} else {
				return;
			}
		} else {
            img.color = Color.white;
        }
	}

    public void OnPressToTest() {
        buttonPressed = true;
        audio.Play();

        if (buttonPressed)
            img.color = Color.red;
    }
}
