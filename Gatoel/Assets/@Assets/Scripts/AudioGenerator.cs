﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class AudioGenerator : MonoBehaviour {

    #region VARIABLES

    public int index;
    [SerializeField] private int noteNumber;
    [SerializeField] private string key;
    [Header("External Components")]
    public FrequencyInitiator fqInit;
    public AudioManager audioManager;
	public UIManager ui;
    public Image image;
    [SerializeField] private AudioSource audioSource;
	[SerializeField] private Instrument listInstrument;
    [Header("Generated Audio Clips")]
    [SerializeField] private AudioClip slendroClip;
    [SerializeField] private AudioClip pelogClip;
    [Header("Components Values")]
	[SerializeField] private float amplitude = 1f;
    [SerializeField] private float frequency = 0f;
    [SerializeField] private float slendroFrequency = 0f;
    [SerializeField] private float pelogFrequency = 0f;
    [SerializeField] private int position = 0;
	[SerializeField] private int sampleLength = 0;
	[SerializeField] private int samplingRate = 0;
    [SerializeField] private float phase = 0f;
    
    private float a = 0f;
    private float b = 0f;
    private float c = 0f;
    private bool isPressed = false;

    #endregion

    #region EVENTS

    private void Awake() {
        sampleLength = audioManager.sampleLength;
        samplingRate = audioManager.samplingRate;
        listInstrument = audioManager.listInstrument;

        audioSource = GetComponent<AudioSource>();

        fqInit.InitFrequency(listInstrument);
    }

    private void Start() {
		if (fqInit.blockProperties != null) {
            foreach (BlockProperties pro in fqInit.blockProperties) {
                if (index == pro.index) {
                    slendroFrequency = pro.slendroFq;
                    pelogFrequency = pro.pelogFq;
                }
            }
        }

        GenerateAudioClip();
    }

    private void Update() {
		switch (audioManager.listScale) {
            case Scale.Slendro:
                frequency = slendroFrequency;
                audioSource.clip = slendroClip;
                break;
            case Scale.Pelog:
                frequency = pelogFrequency;
                audioSource.clip = pelogClip;
                break;
            default:
                return;
        }

    #if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
        if (Input.GetKeyDown(key)) {
            audioSource.volume = (audioManager.volume / 100) * 0.75f;
            audioSource.Play();
        }
    #endif
    }

    #endregion

    #region AUDIO CLIP GENERATOR

    private void GenerateAudioClip() {
        slendroClip = AudioClip.Create("slendro_" + index.ToString(),
            sampleLength, 2,
            samplingRate, true,
            OnAudioRead, OnAudioSetPosition);

        pelogClip = AudioClip.Create("pelog_" + index.ToString(),
            sampleLength, 2,
            samplingRate, true, 
            OnAudioRead, OnAudioSetPosition);
    }

    private void OnAudioRead(float[] data) {
		int dataLength = data.Length;
		
        for (int i = 0; i < dataLength; i++) {
            phase = Mathf.Sin(amplitude * 2 * Mathf.PI * (frequency / samplingRate) * position);
            data[i] = Envelope(listInstrument, phase);
            position++;
        }
    }

    private void OnAudioSetPosition(int newPosition) {
        position = newPosition;
    }

    private float Envelope(Instrument _inst, float _phase) {
        switch (_inst) {
            case Instrument.Saron:
                a = -1.03f * (Mathf.Pow(10, -5)) * position;
                b = Mathf.Exp(a);
                c = 0.7693f;
                break;
            case Instrument.Demung:
                a = -1.194f * (Mathf.Pow(10, -5)) * position;
                b = Mathf.Exp(a);
                c = 0.8223f;
                break;
            case Instrument.Peking:
                a = -1.709f * (Mathf.Pow(10, -5)) * position;
                b = Mathf.Exp(a);
                c = 0.7663f;
                break;
            case Instrument.BonangPenerus:
                a = -3.7966f * (Mathf.Pow(10, -5)) * position;
                b = Mathf.Exp(a);
                c = 0.7319f;
                break;
            case Instrument.BonangBarung:
                a = -3.5624f * (Mathf.Pow(10, -5)) * position;
                b = Mathf.Exp(a);
                c = 0.7235f;
                break;
            default:
                break;
        }
		
		var env = c * b;

        return env * _phase;
    }

    #endregion

    #region AUDIO PLAY HANDLER

	public void OnTouchToPlay() {
		audioSource.volume = (audioManager.volume / 100) * 0.675f;
        audioSource.Play();
    }

    public void OnTouchToPlayBalungan(int _noteNumber) {
        audioSource.volume = (audioManager.volume / 100) * 0.675f;
        noteNumber = _noteNumber;
        Debug.Log(noteNumber);
    }

    public void OnTouchToStop() {
        StartCoroutine(FadeOut(audioSource, 0.1f));

        isPressed = true;
        if (isPressed)
        {
            image.color = Color.red;
            isPressed = false;
        }

        StartCoroutine(ChangeColor(this.image, .5f));
    }

    public void OnTouchToStopBalungan() {

    }

	private static IEnumerator FadeOut(AudioSource source, float fadeTime) {
		float startVolume = source.volume;

		while (source.volume > 0) {
			source.volume -= startVolume * Time.deltaTime / fadeTime;

			yield return null;
		}

		source.Stop();
	}

    private static IEnumerator ChangeColor(Image _image, float _time) {

        for (float t = 0f; t < 1f; t += Time.deltaTime / _time) {
            Color c = _image.color;
            c.a = Mathf.Lerp(1, 0, t);
            _image.color = c;

            yield return null;
        }
    }

    #endregion
}
