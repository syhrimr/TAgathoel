﻿using UnityEngine;

public enum Instrument {
    Saron,
    Peking,
    Demung,
    BonangBarung,
    BonangPenerus,
    Slentem,
    Kempul,
    Kenong,
    Kendang
};

public enum Scale {
    Slendro,
    Pelog
};

public class AudioManager : MonoBehaviour {

    [Header("Main Components")]
    public Instrument listInstrument;
    public Scale listScale;

    [Header("Volume Settings")]
    [Range(0f, 100f)] public float volume = 100f;

    [Header("Wave Initiation")]
    public int sampleLength = 100000;
    public int samplingRate = 48000;
    public float amplitude = 1f;
}