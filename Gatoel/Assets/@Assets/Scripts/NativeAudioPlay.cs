﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class NativeAudioPlay : MonoBehaviour {

    public AudioManager audioManager;
    public AudioSource audioSource;

    #region ANDROID_COMPONENTS
    public AndroidAudioBypass.BypassAudioManager bypassAudioManager;
    public string pelogAudio;
    public string slendroAudio;

    [Range(0f, 1f)] public int priority = 1;
    [Range(0f, 1f)] public float volume = 1f;
    [Range(.5f, 2f)] public float rate = 1f;

    public bool playOnAwake = false;
    public bool loop = false;

    private int soundIdSlendro;
    private int soundIdPelog;
    #endregion

    private static NativeAudioPlay instance;
    public static NativeAudioPlay Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType<NativeAudioPlay>();
            }

            return instance;
        }
    }

    #region EVENTS
    private void Start() {
        if (String.IsNullOrEmpty(slendroAudio) || String.IsNullOrEmpty(pelogAudio)) {
            //Debug.LogError("Audio file not specified in " + gameObject.name);
            return;
        }

        if (audioSource == null)
            return;

        audioSource = GetComponent<AudioSource>();

        soundIdSlendro = bypassAudioManager.RegisterSoundFile(slendroAudio);
        soundIdPelog = bypassAudioManager.RegisterSoundFile(pelogAudio);

        if (playOnAwake) {
            AndroidPlay();
        }
    }
    #endregion

    public void AndroidPlay() {
        if (Application.platform == RuntimePlatform.Android) {
            if (rate < 0.5f)
                rate = 0.5f;

            if (rate > 2.0f)
                rate = 2.0f;

            switch(audioManager.listScale) {
                case Scale.Pelog:
                    bypassAudioManager.PlaySound(soundIdPelog, volume, volume, priority, loop ? -1 : 0, rate);
                    break;
                case Scale.Slendro:
                    bypassAudioManager.PlaySound(soundIdSlendro, volume, volume, priority, loop ? -1 : 0, rate);
                    break;
                default:
                    return;
            }
        }
    }

    public void IosPlay() {
        if (Application.platform == RuntimePlatform.IPhonePlayer) {
            IosNativeAudio.LoadSound();
            IosNativeAudio.PlaySound();
        }
    }
}