﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {

    [Header("UI Elements")]
    public RectTransform panel;
    public RectTransform center;
    public Button[] btn;
    [Header("Configure")]
	[SerializeField] private float[] 	distance;
    [SerializeField] private float[] 	distReposition;
    [SerializeField] private float 		lerpSpeed 		= 5f;
    [SerializeField] private bool 		dragging 		= false;
    [SerializeField] private bool 		messageSend 	= false;
    [SerializeField] private int 		btnDistance	;
    [SerializeField] private int 		minButtonNum;
    [SerializeField] private int 		btnLength;
    [SerializeField] private int 		startButton 	= 1;

    private void Start() {
        btnLength = btn.Length;
        distance = new float[btnLength];
        distReposition = new float[btnLength];

        btnDistance = (int)Mathf.Abs(btn[1].GetComponent<RectTransform>().anchoredPosition.x - btn[0].GetComponent<RectTransform>().anchoredPosition.y);

        panel.anchoredPosition = new Vector2((startButton - 1) * -300f, 0f);
    }

    private void Update() {
		int length = btn.Length;
		
        for (int i = 0; i < length; i++) {
            distReposition[i] 	= center.GetComponent<RectTransform>().position.x - btn[i].GetComponent<RectTransform>().position.x;
            distance[i] 		= Mathf.Abs(distReposition[i]);

            if (distReposition[i] > 20) {
                float curX = btn[i].GetComponent<RectTransform>().anchoredPosition.x;
                float curY = btn[i].GetComponent<RectTransform>().anchoredPosition.y;

                Vector2 newAnchoredPos = new Vector2(curX + (btn.Length * btnDistance), curY);
                btn[i].GetComponent<RectTransform>().anchoredPosition = newAnchoredPos;
            }

            if (distReposition[i] < -20) {
                float curX = btn[i].GetComponent<RectTransform>().anchoredPosition.x;
                float curY = btn[i].GetComponent<RectTransform>().anchoredPosition.y;

                Vector2 newAnchoredPos = new Vector2(curX - (btnLength * btnDistance), curY);
                btn[i].GetComponent<RectTransform>().anchoredPosition = newAnchoredPos;
            }

/*            if (distReposition[i] < 2.5f && distReposition[i] > -2.5f) {
                btn[i].GetComponent<Transform>().localScale = new Vector2(1.15f, 1.15f);
            } else {
                btn[i].GetComponent<Transform>().localScale = new Vector2(1.0f, 1.0f);
            }
*/        }

        float minDistance = Mathf.Min(distance);

        for (int a = 0; a < length; a++) {
            if (minDistance == distance[a]) minButtonNum = a;
        }

        if (!dragging) {
            //LerpToButton(minButtonNum * -btnDistance);
            LerpToButton(-btn[minButtonNum].GetComponent<RectTransform>().anchoredPosition.x);
        }
    }

    private void LerpToButton(float position) {
        float newX = Mathf.Lerp(panel.anchoredPosition.x, position, Time.deltaTime * lerpSpeed);
        
        if (Mathf.Abs(position - newX) < 3f)
            newX = position;


        if (Mathf.Abs(newX) >= Mathf.Abs(position) -1f && Mathf.Abs(newX) <= Mathf.Abs(position) + 4 && !messageSend)
            messageSend = true;

        Vector2 newPosition 	= new Vector2(newX, panel.anchoredPosition.y);
        panel.anchoredPosition 	= newPosition;
    }

    public void StartDrag() {
        //messageSend = false;
        lerpSpeed 	= 5f;
        dragging 	= true;
    }

    public void EndDrag() {
        dragging 	= false;
    }
}