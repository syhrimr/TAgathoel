﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class BasicAudioPlay : MonoBehaviour {

    #region VARIABLES

    [Header("External Components")]
    public AudioManager audioManager;
    public Slider volumeSlider;
	[Header("Volume Setting")]
	[Range(0f, 1f)] 
	[SerializeField] private float volumeLimit;
    [Header("Block Properties")]
    public ButtonProperties[] btnPro;
    [Header("Stop Queue")]
    [SerializeField] private int pressedNote;
    [SerializeField] private bool isPressed = false;

    #endregion
    
    #region EVENTS

    private void Start() {
        audioManager = GetComponent<AudioManager>();
        audioManager.listScale = Scale.Slendro;
    }

    private void Update() {
        if (audioManager.listScale == Scale.Slendro) {
            for (int i = 0; i <= btnPro.Length - 1; i++) {
                btnPro[i].audio.clip = btnPro[i].clips[0];
            }
        }
        else if (audioManager.listScale == Scale.Pelog) {
            for (int i = 0; i <= btnPro.Length - 1; i++) {
                btnPro[i].audio.clip = btnPro[i].clips[1];
            }
        }
    }

    #endregion

    #region AUDIO PLAY HANDLER

    public void OnClickToPlayAudio(int btnIndex) {
        if (Application.platform != RuntimePlatform.Android ||
            Application.platform != RuntimePlatform.IPhonePlayer) {
            for (int i = 0; i <= btnPro.Length - 1; i++) {
                if (btnIndex == btnPro[i].index) {

                    pressedNote = btnIndex;

                    btnPro[i].audio.volume = (audioManager.volume / 100) * volumeLimit;
                    btnPro[i].audio.PlayOneShot(btnPro[i].audio.clip);
                }
            }
        }
    }

    public void OnClickToStopAudio() {
        if (Application.platform != RuntimePlatform.Android ||
            Application.platform != RuntimePlatform.IPhonePlayer) {

            for (int i = 0; i <= btnPro.Length - 1; i++) {
                if (pressedNote == btnPro[i].index)
                    StartCoroutine(FadeOut(btnPro[i].audio, .2f));
            }
        }
    }

    private static IEnumerator FadeOut(AudioSource source, float fadeTime) {
        float startVolume = source.volume;

        while (source.volume > 0) {
            source.volume -= startVolume * Time.deltaTime / fadeTime;

            yield return null;
        }

        source.Stop();
    }

    private static IEnumerator ChangeColor(Image _image, float _time) {

        for (float t = 0f; t < 1f; t += Time.deltaTime / _time) {
            Color c = _image.color;
            c.a = Mathf.Lerp(1, 0, t);
            _image.color = c;

            yield return null;
        }
    }

    #endregion
}